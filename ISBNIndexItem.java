public class ISBNIndexItem {
    private String isbn = new String("");
    private int recordNumber = 0;
    
/****************************************************************************/                                                                  
// Description:  This method is the constructor for the object. It instantiates
//               the object.
//
// Parameters:   i is a string value and r is an int that the object will be 
//               instantiated to.
//
// Returns:      Does not return a value.
//              
// Restrictions: Must be instantiated with a string and an int in that order.
/****************************************************************************/
    public ISBNIndexItem(String i , int r) {
	isbn = i;
	recordNumber = r;
    }
/****************************************************************************/                                                                  
// Description:  This method returns the value of the private variable isbn.
//
// Parameters:   No parameters.
//
// Returns:      Returns isbn.
//              
// Restrictions: No restrictions.
/****************************************************************************/
    public String getISBN(){
	return isbn;
    }
/****************************************************************************/                                                                  
// Description:  This method sets the value of the private variable isbn.
//
// Parameters:   String i is the value isbn will be set to.
//
// Returns:      Does not return a value.
//              
// Restrictions: Paramter must be a string.
/****************************************************************************/
    public void setISBN(String i){
	isbn = i;
    }
/****************************************************************************/                                                                  
// Description:  This method returns the value of the private variable record 
//               number.
//
// Parameters:   No parameters.
//
// Returns:      Returns record number.
//              
// Restrictions: No restrictions.
/****************************************************************************/
    public int getrecordNumber() {
	return recordNumber;
    }
/****************************************************************************/                                                                  
// Description:  This method sets the value of the private variable record 
//               number.
//
// Parameters:   Int r is the value record number will be set to.
//
// Returns:      Does not return a value.
//              
// Restrictions: Paramter must be a int.
/****************************************************************************/
    public void setrecordNumber(int r){
	recordNumber = r;
    }
/****************************************************************************/                                                                  
// Description:  This method returns all value in the object.
//
// Parameters:   No parameters.
//
// Returns:      Returns all the variables in the object.
//              
// Restrictions: No restrictions.
/****************************************************************************/
    public String toString () {
	return isbn + " " + recordNumber;
    }
}
//By: Aleksandar 
//Wednesday May 30, 2012
