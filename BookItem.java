public class BookItem {
    private String title = new String("");
    private String author = new String("");
    private double price = 0.00;
    
/****************************************************************************/                                                                  
// Description:  This method is the constructor for the object. It instantiates
//               the object.
//
// Parameters:   t and a are string values and p is a double value that the 
//               object will be instantiated to.
//
// Returns:      Does not return a value.
//              
// Restrictions: Must be instantiated with two strings and a double in that 
//               order
/****************************************************************************/
    public BookItem(String t, String a, double p) {
	title = t;
	author = a;
	price = p;
    }
/****************************************************************************/                                                                  
// Description:  This method returns the value of the private variable title.
//
// Parameters:   No parameters.
//
// Returns:      Returns title.
//              
// Restrictions: No restrictions.
/****************************************************************************/
    public String getTitle() {
	return title;
    }
/****************************************************************************/                                                                  
// Description:  This method sets the value of the private variable title.
//
// Parameters:   String t is the value title will be set to.
//
// Returns:      Does not return a value.
//              
// Restrictions: Paramter must be a string.
/****************************************************************************/     
    public void setTitle (String t) {
	title = t;
    }
/****************************************************************************/                                                                  
// Description:  This method returns the value of the private variable author.
//
// Parameters:   No parameters.
//
// Returns:      Returns author.
//              
// Restrictions: No restrictions.
/****************************************************************************/
    public String getAuthor () {
	return author;
    }   
/****************************************************************************/                                                                  
// Description:  This method sets the value of the private variable author.
//
// Parameters:   String a is the value author will be set to.
//
// Returns:      Does not return a value.
//              
// Restrictions: Paramter must be a string.
/****************************************************************************/
     public void setAuthor (String a) {
	author = a;
    }
/****************************************************************************/                                                                  
// Description:  This method returns the value of the private variable price.
//
// Parameters:   No parameters.
//
// Returns:      Returns price.
//              
// Restrictions: No restrictions.
/****************************************************************************/
    public double getPrice() {
	return price;
    }
/****************************************************************************/                                                                  
// Description:  This method sets the value of the private variable price.
//
// Parameters:   Double p is the value price will be set to.
//
// Returns:      Does not return a value.
//              
// Restrictions: Paramter must be a double.
/****************************************************************************/   
    public void setPrice (double p) {
	price = p;
    }
/****************************************************************************/                                                                  
// Description:  This method returns all value in the object.
//
// Parameters:   No parameters.
//
// Returns:      Returns all the variables in the object.
//              
// Restrictions: No restrictions.
/****************************************************************************/
    public String toString(){
	return (title + ",    " + author + ",    " + price);
    }
}    
//By: Aleksandar 
//Thursday May 30, 2012
 
