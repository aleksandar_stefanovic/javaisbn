import java.io.*;
import hsa.*;
public class ISBNmain {
    public static int BOOK_LIST_SIZE = 100;
    public static void main (String[] args) { 
	String key = new String();
	int totalItems = 0; 
	int j = 0;
	ISBNIndexItem[] indexItems = new ISBNIndexItem [BOOK_LIST_SIZE];
	BookItem[] bookData = new BookItem [BOOK_LIST_SIZE];
	initializeBookItem (bookData);
	initializeIsbnIndexItem (indexItems);
	totalItems = readISBNFileIntoArray (indexItems);
	totalItems = readBookDataFileIntoArray(bookData);
	printIsbnIndex (indexItems);
	printBookData (bookData);
	System.out.println("\nPlease enter a ISBN(without hypens): ");
	key = Stdin.readLine(); 
	while(!key.equals("999")) {
	    j = binarySearch(indexItems, totalItems, key);
	    if (j == -1)
		System.out.println("Entry not in list.");
	    else {
		j = indexItems[j].getrecordNumber()-1;
		System.out.println(bookData[j].getTitle() + ", " + bookData[j].getAuthor() + ", " + bookData[j].getPrice()); 
	    }
	    System.out.println("\nPlease enter an ISBN(without hypens): ");
	    key = Stdin.readLine(); 
	}
    }

/****************************************************************************/                                                                  
// Description:  This method initializes the indexItems array that is passed
//               through its parameter.
//
// Parameters:   indexItems is the array to be initialized
//
// Returns:      Does not return a value.
//              
// Restrictions: The array passed must be of type ISBNIndexItem and previously
//               declared.
/****************************************************************************/ 
    private static void initializeIsbnIndexItem (ISBNIndexItem[] indexItems) {
	for (int i = 0 ; i < BOOK_LIST_SIZE ; i++)
	    indexItems [i] = new ISBNIndexItem ("", -1);
    }

/****************************************************************************/                                                                  
// Description:  This method initializes the bookData array that is passed
//               through its parameter.
//
// Parameters:   bookData is the array to be initialized.
//
// Returns:      Does not return a value.
//              
// Restrictions: The array passed must be of type BookItem and be previously
//               declared.
/****************************************************************************/
    private static void initializeBookItem (BookItem[] bookData) {
	for (int i = 0 ; i < BOOK_LIST_SIZE ; i++)
	    bookData [i] = new BookItem ("", "", 0.00);
    }
/****************************************************************************/                                                                  
// Description:  This method prints the indexItems array that is passed
//               through its parameter.
//
// Parameters:   indexItems is the array to be initialized
//
// Returns:      Does not return a value.
//              
// Restrictions: The array passed must be the indexItems array.
/****************************************************************************/
    private static void printIsbnIndex (ISBNIndexItem[] indexItems) {
	for (int i = 0; i < BOOK_LIST_SIZE; i++) {
	    System.out.println(indexItems[i]);
	}
    }
/****************************************************************************/                                                                  
// Description:  This method prints the bookData array that is passed
//               through its parameter.
//
// Parameters:   bookData is the array to be initialized
//
// Returns:      Does not return a value.
//              
// Restrictions: The array passed must be the bookData array.
/****************************************************************************/
    private static void printBookData (BookItem[] bookData) {
	for (int i = 0; i < BOOK_LIST_SIZE; i++) {
	    System.out.println(bookData[i]);
	}
    }

/****************************************************************************/                                                                  
// Description:  This method reads ISBN and record numbers from a file into 
//               the indexItems array passed through its parameter.
//
// Parameters:   indexItems is the array to be initialized
//
// Returns:      The number of entries read.
//              
// Restrictions: The array passed must be of type ISBNIndexItem and be 
//               previously declared. The file containing ISBN codes must be
//               named ISBNIndex.txt and formatted so that a single ISBN and
//               record number appear on each line.        
/****************************************************************************/
    public static int readISBNFileIntoArray (ISBNIndexItem[] indexItems) {
	FileInputStream src;
	String lineRead = new String ("");
	int count = 0;
	try {
	    src = new FileInputStream ("ISBNIndex.txt");
	    lineRead = readLine(src);
	    while (!lineRead.equals("")) {
	    //Loop to keep reading from the file until the readLine method returns a null
		indexItems[count].setrecordNumber (Integer.parseInt (lineRead.substring(17, 22)));
		//Substrings the final digit from the code to be used as the record number
		indexItems[count].setISBN (lineRead.substring(0,3) + lineRead.substring(4,5) + lineRead.substring(6,13) + lineRead.substring(14,15) + lineRead.substring(16,17));
		//Substrings the ISBN code without dashes to be used as the ISBN code
		count++;
		lineRead = readLine(src);
	    }
	    src.close();
	}
	catch (IOException e) {
	    System.out.println(e.getMessage());
	}
	return count;
    }
/****************************************************************************/                                                                  
// Description:  This method reads book titles, authors, and prices from a 
//               file into the bookData array passed through its parameter.
//               
//
// Parameters:   bookData is the array to be initialized
//
// Returns:      The number of entries read.
//              
// Restrictions: The array passed must be of type BookData and be 
//               previously declared. The file containing book data must be
//               named BookData.txt and formatted so that a single title, 
//               author, and price for each bokk appear on seperate lines.           
/****************************************************************************/
    public static int readBookDataFileIntoArray(BookItem[] bookData) {
	FileInputStream src;
	String lineRead = new String ("");
	int count = 0;
	try {
	    src = new FileInputStream ("BookData.txt");
	    lineRead = readLine(src);
	    while (!lineRead.equals("")) {
	    //Loop to keep reading from the file until the readLine method returns a null
		bookData[count].setTitle(lineRead);
		lineRead = readLine(src);
		bookData[count].setAuthor(lineRead);
		lineRead = readLine(src);
		bookData[count].setPrice (parseDouble(lineRead));
		lineRead = readLine(src);
		count++;
	    }
	    src.close();
	}
	catch (IOException e) {
	    System.out.println(e.getMessage());
	}
	return count;
   
    }


/****************************************************************************/                                                                  
// Description:  This method reads a line from the file passed in the     
//               parameter f and returns it as a String.
//
// Parameters:   f is the file to read from
//
// Returns:      The next line of text from the file f
//               Null, i.e., "" if the end of file has been reached
//              
// Restrictions: The file f must have been previously opened
/****************************************************************************/ 
    private static String readLine (FileInputStream f) {
	String line = new String("");
	try {
	    int c = f.read();
	    if (c != -1)
		line = (char)c + "";
	    while (c != -1 && c != '\n') {
		c = f.read();
		if (c != -1 && c != '\n')
		    line = line + (char)c;
	    }
	}
	catch (IOException e) {
	    System.out.println(e.getMessage());
	}
	return line.trim();
    }
/****************************************************************************/                                                                  
// Description:  This method converts a string to a double
//
// Parameters:   n is the string to be converted.
//
// Returns:      The string n as a double.
//              
// Restrictions: The string n must be less than 18 digits long, and must 
//               contain a decimal point.
/****************************************************************************/
    private static double parseDouble (String n) {
	int r = n.indexOf(".");//Determines the index of position of the decimal
	int l = n.length() - r - 1; 
	double b = (double)(Integer.parseInt(n.substring(0,r)));
	//Turns the part before the decimal into a double through casting
	double g = ((double)(Integer.parseInt(n.substring(r+1,n.length())))/Math.pow(10,l));
	//Turns the part after the decimal into a double and divides it by the neccesary amount
	//to make it a decimal.
	double y = b+g; //Adds both parts together
	return y;
    }
    
/*********************************************************************************************/
// Header:       int binarySearch (ISBNIndexItem[] indexItems, int numberOfItems, String key)
//                                                               
// Description:  This method searches for an isbn number in the indexItems
//               array that matches the key passed as parameter. 
//
// Parameters:   indexItems is the array to search
//               key is the item we are searching for
//               numberOfItems is the number of isbn numbers in the array
//
// Returns:      The position of the found item. The first element is zero
//               If the sought item is not found, the method returns -1
//              
// Restrictions: The array indexItems must be sorted
/*********************************************************************************************/

    public static int binarySearch (ISBNIndexItem[] indexItems, int numberOfItems, String key) {
	int bottom = 0;
	int top = numberOfItems - 1;
	int index = (bottom + top) / 2;
	while (!indexItems [index].getISBN ().trim ().equals (key) && bottom <= top) {
	    if (key.compareTo (indexItems [index].getISBN ()) > 0)
		bottom = index + 1;
	    else
		top = index - 1;
	    index = (bottom + top) / 2;
	}
	if (bottom > top)
	    return -1;
	else
	    return index;   
    }
}

//By Aleksandar
//Thursday May 30, 2012

